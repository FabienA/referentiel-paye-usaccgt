#Construction d'une docs ReadTheDoc
FROM debian:11

#MAJ & python 3
RUN apt-get update && apt-get upgrade -y && apt-get install -y python3 python3-pip && apt-get clean;

#Installation Sphinx & extension
RUN pip3 install sphinx sphinx_rtd_theme myst-parser sphinx-tabs

#Répertoire où sera monté les données du projet
RUN mkdir /project

WORKDIR /project

#Commande pour build la version html
ENTRYPOINT ["make"]
CMD ["html"]
