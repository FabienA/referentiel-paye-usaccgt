# Template pour de la documentation basé sur sphynx (ReadTheDoc Theme)

copyright JiCambe, utilisation libre & aucune garantie.

Le principal intéret du template est qu'il permet l'utilisation d'un contener Docker/Podman pour la génération de la doc en local.

## Pré-requis
Docker ou podman d'installé.
Si vous utilisez podman, pensez à ajouter l'alias docker=podman

## Installation
Pour builder l'image docker à utiliser, utiliser le script `setup.bat` ou `setup.sh`

## Générer la doc
Lancez le script `build.bat` ou `build.sh`

## Ecrire la doc
Au format markdown ou rest en utilisant les réglages et convention de ReadTheDoc et Sphynx

## Extension / Theme (sphynx) installé
- ReadTheDoc theme : https://sphinx-rtd-theme.readthedocs.io/en/stable/index.html
- myst-parser : https://myst-parser.readthedocs.io/en/latest/intro.html