# Traitement indiciaire

**Mise à jour** : 5/6/2023

Le traitement de base ou traitement brut indiciaire est l'élément principal de la rémunération d'un fonctionnaire.
Il est déterminé à partir de le grille indiciaire de votre corps & grade.

## Résumé

:Nom: Traitement indiciaire 
:Code: 101000
:Elements: TRAITEMENT BRUT
:Montant versé: oui, voir {ref}`elements/remuneration_de_base/traitement:mode de calcul`
:Part Salariale: N/A
:Part Patronale: N/A

## Cas d'usage
Cet élément est tout le temps présent sur la feuille de paye

## Paramètres nécéssaires
*(Règlage de l'application)*

* {ref}`{TIB100} <TIB100>` : valeur du traitement indiciaire brut annuel de l'indice majoré 100

:::{note}
La valeur ci-dessus est celle utilisé dans le calcul officiel. Couramment, il est fait référence à la valeur du point d'indice qui est cette valeur divisé par 1200.
:::

## Données nécéssaires
*(dépend du cas de l'agent)*

* {ref}`{IM} <IM>` : indice majoré actuel de l'agent. Cette valeur est déduite du corps et du grade de l'agent et dépend des grilles indiciaires.
* {DEP} : Département/Territoire du lieu d'affectation.

## Mode de calcul

Montant versé = {ref}`{IM} <IM>` * {ref}`{TIB100} <TIB100>` / 1200 \
Les millièmes d'euros sont ignorés.


## Cas Particuliers

### Cas 1 : Fonctionnaire affecté en "Département d'Outre Mer" (sauf Mayotte)

#### Conditions (cas 1)

Être en service en Guadeloupe, en Guyane, à la Martinique, à La Réunion, à Saint-Barthélemy, à Saint-Martin et à Saint-Pierre-et-Miquelon. (voir {DEP})

#### Calcul (cas 1)

Montant versé = Montant versé (cas général) * 1,25

(voir [Article L741-1 du CGFP](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044424665))

### Cas 2 : Fonctionnaire affecté à Mayotte

#### Conditions (cas 2)

Être en service à Mayotte. (voir {DEP})

#### Calcul (cas 2)

Montant versé = Montant versé (cas général) * 1,40

*(voir [Article L741-1 du CGFP](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044424665))*


### Cas 3 : Fonctionnaire affecté en TOM (Wallis et Futuna, en Polynésie française, en Nouvelle-Calédonie et aux Terres australes et antarctiques françaises)

#### Conditions (cas 3)

Être en service à Wallis et Futuna, en Polynésie française, en Nouvelle-Calédonie ou aux Terres australes et antarctiques françaises. (voir {DEP})

#### Calcul (cas 3)

:::{attention}
En fait, c'est compliqué et il faudrait une vraie fiche de paye pour savoir : 

*La rémunération à laquelle peuvent prétendre les magistrats et fonctionnaires visés à l'article premier du présent décret, lorsqu'ils sont en position de service, est égale au traitement afférent à l'indice hiérarchique détenu dans l'emploi occupé, augmenté de l'indemnité de résidence et du supplément familial de traitement qu'ils percevraient s'ils étaient en service à Paris, l'ensemble étant multiplié par un coefficient de majoration propre à chaque territoire.*
Voir : https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000338682

Je n'ai pas trouvé les arrêtés en vigueurs avec le coefficient actuel.
:::

*(voir [Article L742-1 du CGFP](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044424659) et article 2 de la loi n° 50-772 du 30 juin 1950

## Références

* [Service Public : Traitement indiciaire dans la fonction publique](https://www.service-public.fr/particuliers/vosdroits/F461)
* [Décret N°85-1148 du 24/10/1985 actualisé (légifrance)](https://www.legifrance.gouv.fr/loda/id/LEGITEXT000006064738/)
* [Code général de la fonction publiqu, Livre VII, Titre IV dispositions particulières relatives à l'outre-mer (légifrance)](https://www.legifrance.gouv.fr/codes/section_lc/LEGITEXT000044416551/LEGISCTA000044423909/#LEGISCTA000044424669)
* [loi n° 50-772 du 30 juin 1950 fixant les soldes dans les TOM (légifrance)](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000886443/)
* [Décret N°67-600 du 23 juillet 1967 régime de rémunération sur les TOM (Légifrance)](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000338682)


Fiche élément : 
  Nom: 
  
  Lignes correspondante fiche de paye : 
    - code:
	  libelle: (colonne élément)
	  ce que c'est:   Montant versé (= à payer) / Part salariale (= à déduire) / Part Patronale (=Pour information)

  Cas d'usage: (dans quel cas l'élément figurera sur la fiche de paye)

  Paramètres nécéssaire : (Un paramètre est une valeur qui ne dépend pas du cas de l'utilisateur. ex : valeur du point d'indice)

  Données nécéssaires : (valeur qui dépende du cas de l'utilisateur. Ce n'est pas forcément la question directe qui lui sera posé. ex : Indice Brut --> La question posé à l'utilisateur est son grade-échelon, l'IB est déduite de la réponse de l'utilisateur à cette question.)

  Calcul (cas général) : 

  Cas particulier :
    - cas 1 (résumé)
	  condition : (condition de déclenchement de ce cas particulier. ex : agent en guyanne)
	  calcul : 
	- cas 2 (résumé)
	  condition : (condition de déclenchement de ce cas particulier. ex : agent en guyanne)
	  calcul : 