Référentiel paye USACcgt
================================

Le but de ce document est de servir de référence concernant les bulletins de paye DGAC, tant pour les agents, que pour les codeurs du simulateur de paye USACcgt.

.. toctree::
   :maxdepth: 2
   :caption: L'essentiel du Bulletin de Paye:
   :glob:

   essentiel/*

.. toctree::
   :maxdepth: 2
   :caption: Données Utilisateurs:

   user_datas/questions.md
   user_datas/donnees_deduites.rst

.. toctree::
   :maxdepth: 2
   :caption: Paramètres:
   
   params/params.md
   params/grilles.md

.. toctree::
   :maxdepth: 2
   :caption: Elements de paye:
   
   elements/remuneration.rst
   elements/primes.rst
   elements/cotisations.rst

.. toctree::
   :maxdepth: 2
   :caption: Aide:
   
   how_to_update.rst
   syntaxe.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
