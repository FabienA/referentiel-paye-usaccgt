# Les grilles indiciaires

Cette pages référence l'ensemble des grilles indiciaires de la DGAC

:Référence législative: [Décret N°2022-994 du 7/1/2022](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000046026212) \
:Vérifié le: 5/6/2023


## Emploi fonctionnel

### CUTAC

### CSTAC

### CTAC

## Corps Administratif DGAC

### ADAAC

### ASAAC

## Corps Techniques DGAC

### TSEEAC

### IESSA

| Grade | Echelon | Durée | Indice Brut |
|:-----:|:-------:|:-----:|:-----------:|
| en chef | 6.3 | - | HEA 3|
|         | 6.2 | - | HEA 2|
|         | 6.1 | - | HEA 1|
| | 5 | ...

|élève |unique| 1 an|340|



:Référence législative: [Décret N°91-56 du 16 janvier 1991 (le statut)](https://www.legifrance.gouv.fr/loda/id/LEGITEXT000006077105) & [Décret n° 2009-1322 du 27 octobre 2009 (les indices)](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000021213692/) & [(corespondance IB<->IM)] ()
:Vérifié le: 5/6/2023

### ICNA

| Grade | Echelon | Durée | Indice Brut |
|:-----:|:-------:|:-----:|:-----------:|
|en chef| 7e | - |HEA|
| | 6e | 1 an | 1027|
| | 5e | 1 an 6 mois|995|
| | 4e | 1 an 6 mois|946|
| | 3e | 1 an 6 mois|890|
| | 2e | 2 ans | 837|
| | 1er | 2 ans | 784 |
|divisionnaire|14e|-|1027|
| |13e|2 ans|995|
| |12e|2 ans|946|
| |11e|2 ans|890|
| |10e|2 ans|837|
| | 9e|2 ans|784|
| | 8e|2 ans|741|
| | 7e|2 ans|699|
| | 6e|2 ans|661|
| | 5e|2 ans|615|
| | 4e|2 ans|573|
| | 3e|2 ans|538|
| | 2e|2 ans|501|
| |1er|1 an|471|
|normal| 9e|-|689|
| | 8e|3 ans|661|
| | 7e|3 ans|635|
| | 6e|3 ans|618|
| | 5e|3 ans|572|
| | 4e|3 ans|535|
| | 3e|2 ans|500|
| | 2e|2 ans|469|
| |1er|2 ans|444|
|stagiaire| 2e| |359|
| |1er| |359|
|élève |unique| |340|


:Référence législative: [Décret n°90-998 du 8 novembre 1990 (le statut)](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000708880) & [Décret n° 2009-1322 du 27 octobre 2009 (les indices)](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000021213692/)

### IEEAC

## Corps Ministériel