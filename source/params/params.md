# Paramètres généraux

(TIB100)=
{TIB100}
: valeur du traitement indiciaire brut annuel de l'indice majoré 100

  | Date     |  Valeur |
  |:--------:|:-------:|
  | 1/7/2022 | 5820€04 |
  
  **Référence législative:** [Article 3 du décret n°85-1148 du 24 octobre 1985](https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000046027409) \
  **Vérifié le:** 5/6/2023